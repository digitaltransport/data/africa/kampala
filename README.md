# Kampala

GIS, GTFS and Transit maps for the city of Kampala's public transport network.

This data was collected by [MapUganda](https://mapuganda.org/) and [Transport for Cairo](https://transportforcairo.com/) under a consortium led by [Transitec](https://transitec.net/en/) during late 2019 to early 2020 for the "‘Study of the Paratransit Transport System and Street Usage in the Kampala Metropolitan Area" project sponsored by the French Development Agency.

## Contents:

- **AfD_GKMA-Transit_Data_Codebook.pdf**: Explanation of datasets and how to use and open them

- **GIS**: GIS layers of the network including processed trips from raw field data, stops, stages (terminals), and a list of unique trips

- **GTFS**: A GTFS feed of the network based on the GIS and temporal data gathered from the field

- **Map**: Two tranist maps designed by Transport for Cairo, one for intercity Kampala routes and one for Intracity routes, both simplified to show significant routes